<?php

namespace Drupal\commerce_store_override;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the Store override manager interface.
 */
interface StoreOverrideManagerInterface {

  /**
   * Gets the list of fields that can be overridden.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $bundle_entity
   *   The bundle entity.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   The list of fields that can be overridden.
   */
  public function getPossibleFields(ConfigEntityInterface $bundle_entity);

  /**
   * Gets the list of fields that should be overridden.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $bundle_entity
   *   The bundle entity.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   The list of fields that should be overridden.
   */
  public function getAllowedFields(ConfigEntityInterface $bundle_entity);

  /**
   * Checks whether the given entity should be overridden.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   TRUE if the entity should be overridden, FALSE otherwise.
   */
  public function shouldOverride(ContentEntityInterface $entity);

  /**
   * Overrides the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   */
  public function override(ContentEntityInterface $entity);

}
