<?php

namespace Drupal\commerce_store_override;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\commerce_store\CurrentStoreInterface;

/**
 * Defines the Store override manager.
 */
class StoreOverrideManager implements StoreOverrideManagerInterface {

  /**
   * The current store.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected $currentStore;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The store override repository.
   *
   * @var \Drupal\commerce_store_override\StoreOverrideRepositoryInterface
   */
  protected $repository;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new StoreOverrideManager object.
   *
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   *   The current store.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\commerce_store_override\StoreOverrideRepositoryInterface $repository
   *   The store override repository.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(CurrentStoreInterface $current_store, RouteMatchInterface $route_match, StoreOverrideRepositoryInterface $repository, EntityFieldManagerInterface $entity_field_manager) {
    $this->currentStore = $current_store;
    $this->routeMatch = $route_match;
    $this->repository = $repository;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleFields(ConfigEntityInterface $bundle_entity) {
    $skip_fields = [
      'langcode', 'status', 'created', 'stores', 'variations',
    ];
    $bundle_of = $bundle_entity->getEntityType()->getBundleOf();
    $fields = $this->entityFieldManager->getFieldDefinitions($bundle_of, $bundle_entity->id());
    foreach ($fields as $field_name => $definition) {
      if (in_array($field_name, $skip_fields)) {
        unset($fields[$field_name]);
      }
      if ($definition->isComputed() || !$definition->isDisplayConfigurable('form')) {
        unset($fields[$field_name]);
      }
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowedFields(ConfigEntityInterface $bundle_entity) {
    $field_names = $bundle_entity->getThirdPartySetting('commerce_store_override', 'fields', []);
    if (empty($field_names)) {
      return [];
    }
    $bundle_of = $bundle_entity->getEntityType()->getBundleOf();
    $fields = $this->entityFieldManager->getFieldDefinitions($bundle_of, $bundle_entity->id());
    foreach ($fields as $field_name => $definition) {
      if (!in_array($field_name, $field_names)) {
        unset($fields[$field_name]);
      }
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldOverride(ContentEntityInterface $entity) {
    if (!in_array($entity->getEntityTypeId(), StoreOverride::SUPPORTED_ENTITY_TYPES)) {
      return FALSE;
    }
    $route = $this->routeMatch->getRouteObject();
    if (!$route) {
      // The route isn't available yet (method called during route enhancing).
      return FALSE;
    }
    if ($route->getOption('_admin_route')) {
      // It is assumed that admin routes are used for editing data, in
      // which case the original (master) data should always be shown.
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function override(ContentEntityInterface $entity) {
    $store = $this->currentStore->getStore();
    $store_override = $this->repository->load($store, $entity);
    if ($store_override && $store_override->getStatus()) {
      $store_override->apply($entity);
    }
  }

}
