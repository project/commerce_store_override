<?php

namespace Drupal\Tests\commerce_store_override\Kernel;

use Drupal\Core\Routing\RouteMatch;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductType;
use Drupal\commerce_store_override\StoreOverride;
use Drupal\commerce_store_override\StoreOverrideManager;

/**
 * @coversDefaultClass \Drupal\commerce_store_override\StoreOverrideManager
 * @group commerce
 */
class StoreOverrideManagerTest extends CommerceKernelTestBase {

  /**
   * The store override repository.
   *
   * @var \Drupal\commerce_store_override\StoreOverrideRepositoryInterface
   */
  protected $repository;

  /**
   * A test product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_product',
    'commerce_store_override',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_product');
    $this->installConfig(['commerce_product']);
    $this->installSchema('commerce_store_override', ['commerce_store_override']);

    $this->repository = $this->container->get('commerce_store_override.repository');

    $product = Product::create([
      'type' => 'default',
      'title' => 'Test',
    ]);
    $product->save();
    $this->product = $product;
  }

  /**
   * @covers::getPossibleFields
   * @covers::getAllowedFields
   */
  public function testFields() {
    $product_type = ProductType::load('default');
    /** @var \Drupal\commerce_store_override\StoreOverrideManagerInterface $override_manager */
    $override_manager = $this->container->get('commerce_store_override.manager');
    $possible_fields = $override_manager->getPossibleFields($product_type);
    $this->assertArrayHasKey('title', $possible_fields);
    $this->assertArrayHasKey('body', $possible_fields);
    $this->assertArrayNotHasKey('product_id', $possible_fields);
    $this->assertArrayNotHasKey('langcode', $possible_fields);
    $this->assertArrayNotHasKey('variations', $possible_fields);

    $allowed_fields = $override_manager->getAllowedFields($product_type);
    $this->assertEmpty($allowed_fields);
    $product_type->setThirdPartySetting('commerce_store_override', 'fields', ['title']);
    $product_type->save();
    $allowed_fields = $override_manager->getAllowedFields($product_type);
    $this->assertCount(1, $allowed_fields);
    $this->assertArrayHasKey('title', $allowed_fields);
  }

  /**
   * @covers ::shouldOverride
   */
  public function testShouldOverride() {
    $current_store = $this->container->get('commerce_store.current_store');
    $entity_field_manager = $this->container->get('entity_field.manager');
    $route_provider = $this->container->get('router.route_provider');
    $routes = [
      'entity.commerce_product.edit_form' => FALSE,
      'entity.commerce_product.canonical' => TRUE,
    ];
    foreach ($routes as $route_name => $result) {
      $route = $route_provider->getRouteByName($route_name);
      $route_match = new RouteMatch($route_name, $route);
      $manager = new StoreOverrideManager($current_store, $route_match, $this->repository, $entity_field_manager);

      // Confirm that an unsupported entity type can never be overridden.
      $this->assertFalse($manager->shouldOverride($this->store));
      // Confirm that a supported entity type can be overridden depending
      // on the current route.
      $this->assertEquals($result, $manager->shouldOverride($this->product));
    }
  }

  /**
   * @covers ::override
   */
  public function testOverride() {
    $definition = [
      'data' => [
        'title' => [
          'value' => 'Overridden test1',
        ],
      ],
      'status' => TRUE,
    ];
    $store_override = StoreOverride::create($this->store, $this->product, $definition);
    $this->repository->save($store_override);

    $second_store = $this->createStore('Second store', 'admin@example.com', 'online', FALSE);
    $definition = [
      'data' => [
        'title' => [
          'value' => 'Overridden test2',
        ],
      ],
      'status' => TRUE,
    ];
    $store_override = StoreOverride::create($second_store, $this->product, $definition);
    $this->repository->save($store_override);

    $manager = $this->container->get('commerce_store_override.manager');
    $manager->override($this->product);
    // Confirm that the default store's override was applied.
    $this->assertEquals('Overridden test1', $this->product->label());

    $second_store->setDefault(TRUE)->save();
    $this->container->set('commerce_store.current_store', NULL);
    $this->container->set('commerce_store_override.manager', NULL);

    $manager = $this->container->get('commerce_store_override.manager');
    $manager->override($this->product);
    // Confirm that the new default store's override was applied.
    $this->assertEquals('Overridden test2', $this->product->label());

    // Confirm that the override is no longer applied if it is inactive.
    $this->product = $this->reloadEntity($this->product);
    $definition = [
      'data' => [
        'title' => [
          'value' => 'Overridden test2',
        ],
      ],
      'status' => FALSE,
    ];
    $store_override = StoreOverride::create($second_store, $this->product, $definition);
    $this->repository->save($store_override);
    $manager->override($this->product);
    $this->assertEquals('Test', $this->product->label());
  }

}
