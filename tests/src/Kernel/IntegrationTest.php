<?php

namespace Drupal\Tests\commerce_store_override\Kernel;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_store_override\StoreOverride;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Tests the integration with products and stores.
 *
 * @group commerce
 */
class IntegrationTest extends CommerceKernelTestBase {

  /**
   * The store override repository.
   *
   * @var \Drupal\commerce_store_override\StoreOverrideRepositoryInterface
   */
  protected $repository;

  /**
   * Test products.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface[]
   */
  protected $products = [];

  /**
   * Another test store.
   *
   * @var \Drupal\commerce_store\Entity\StoreInterface
   */
  protected $anotherStore;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_product',
    'commerce_store_override',
    'language',
    'content_translation',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_product');
    $this->installConfig(['commerce_product']);
    $this->installSchema('commerce_store_override', ['commerce_store_override']);

    $this->repository = $this->container->get('commerce_store_override.repository');
    $this->anotherStore = $this->createStore('Another store', 'admin2@example.com');

    ConfigurableLanguage::createFromLangcode('fr')->save();
    ConfigurableLanguage::createFromLangcode('de')->save();

    $first_product = Product::create([
      'type' => 'default',
      'title' => 'Test1',
    ]);
    $first_product->save();

    $second_product = Product::create([
      'type' => 'default',
      'title' => 'Test2',
    ]);
    $second_product->save();
    $this->products = [$first_product, $second_product];

    $store_override = StoreOverride::create($this->store, $first_product, []);
    $this->repository->save($store_override);

    $store_override = StoreOverride::create($this->store, $second_product, []);
    $this->repository->save($store_override);

    $store_override = StoreOverride::create($this->anotherStore, $first_product, []);
    $this->repository->save($store_override);

    $store_override = StoreOverride::create($this->anotherStore, $second_product, []);
    $this->repository->save($store_override);
  }

  /**
   * Confirms that deleting a store deletes matching overrides.
   */
  public function testStoreDelete() {
    $this->store->delete();

    $query = \Drupal::database()
      ->select('commerce_store_override')
      ->fields('commerce_store_override');
    $results = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    $this->assertCount(2, $results);
    $this->assertEquals($this->products[0]->id(), $results[0]['entity_id']);
    $this->assertEquals($this->anotherStore->id(), $results[0]['store_id']);
    $this->assertEquals($this->products[1]->id(), $results[1]['entity_id']);
    $this->assertEquals($this->anotherStore->id(), $results[1]['store_id']);
  }

  /**
   * Confirms that deleting an entity deletes matching overrides.
   */
  public function testEntityDelete() {
    $this->products[1]->delete();

    $query = \Drupal::database()
      ->select('commerce_store_override')
      ->fields('commerce_store_override');
    $results = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    $this->assertCount(2, $results);
    $this->assertEquals($this->store->id(), $results[0]['store_id']);
    $this->assertEquals($this->products[0]->id(), $results[0]['entity_id']);
    $this->assertEquals($this->anotherStore->id(), $results[1]['store_id']);
    $this->assertEquals($this->products[0]->id(), $results[1]['entity_id']);
  }

  /**
   * Confirms that deleting an entity translation deletes matching overrides.
   */
  public function testEntityTranslationDelete() {
    $first_translation = $this->products[0]->addTranslation('fr', [
      'title' => 'Le Test1',
    ]);
    $second_translation = $this->products[0]->addTranslation('de', [
      'title' => 'Le Test1',
    ]);
    $this->products[0]->save();

    $store_override = StoreOverride::create($this->store, $first_translation, []);
    $this->repository->save($store_override);

    $store_override = StoreOverride::create($this->store, $second_translation, []);
    $this->repository->save($store_override);

    $query = \Drupal::database()
      ->select('commerce_store_override')
      ->fields('commerce_store_override')
      ->condition('store_id', $this->store->id())
      ->condition('entity_id', $this->products[0]->id());
    $results = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    $this->assertCount(3, $results);
    $this->assertEquals('de', $results[0]['langcode']);
    $this->assertEquals('fr', $results[1]['langcode']);
    $this->assertEquals(LanguageInterface::LANGCODE_DEFAULT, $results[2]['langcode']);

    $first_translation->delete();
    $query = \Drupal::database()
      ->select('commerce_store_override')
      ->fields('commerce_store_override')
      ->condition('store_id', $this->store->id())
      ->condition('entity_id', $this->products[0]->id());
    $results = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    $this->assertCount(2, $results);
    $this->assertEquals('de', $results[0]['langcode']);
    $this->assertEquals(LanguageInterface::LANGCODE_DEFAULT, $results[1]['langcode']);
  }

}
